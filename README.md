# VaraderoDisplay

This project aims to create a replacement display for Honda Varadero 125 (XL125V) '01-'06, as the factory ones go bad over time and there is no OEM replacement.

As this does NOT aim to replace the whole dashboard and it doesn't interfere with the built-in odometer,
it should not cause problems during official periodic vehicle inspections (think DMV, DVLA, DoT, StrVB, or such).

* Main repo: https://gitlab.com/mjbogusz/varaderodisplay
* Mirror: TBD (github)
* Status: research phase

## Project structure

* `pcb` - hardware design (KiCAD)
* `firmware` (TBD) - firmware for the embedded MCU
* `docs` (TBD) - documentation of the project - how it's built, how to replicate, how to install...
	* note: this may be moved to project's wiki, pages or whatnot
	* `docs/install` - how to install
	* `docs/research` - photos, datasheets, other materials

## License

Details are TBD

hardware design: tentatively CERN-OHL-S-2.0

everything else (firmware, documentation etc): MIT
